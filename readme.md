# elf-ev.de

## Development

This project is using [Middleman](https://middlemanapp.com/) to generate a static website.

It is advised to use [rbenv](https://github.com/rbenv/rbenv) to select the right Ruby version for this project.
If you are using something else you can check the [.ruby-version](.ruby-version) file for the correct Ruby version.

The Gems are managed using bundler. You can run `bundle install` from inside this repository to install the necessary dependencies.

## Deployment

This project is using [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/) to deploy this website to [elf-ev.de](https://elf-ev.de/).

The GitLab pages asset is generated using a [GitLab CI/CD pipeline](.gitlab-ci.yml).
The pipeline is using the default `build` command from `Middleman` with the addition that the build directory is called `public`, so that it can be picked up by GitLab pages.
