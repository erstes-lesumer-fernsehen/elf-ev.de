(function($)
{
  $(function()
  {
    $('.sidenav').sidenav();
  }); // end of document ready
})(jQuery); // end of jQuery name space

document.addEventListener('DOMContentLoaded', function() 
{
    var elems = document.querySelectorAll('.parallax');
    var instances = M.Parallax.init(elems);
});

// Or with jQuery
$(document).ready(function()
{
    $('.parallax').parallax();
});

document.addEventListener('DOMContentLoaded', function() 
{
    var elems = document.querySelectorAll('.carousel');
    var instances = M.Carousel.init(elems);
});

$(document).ready(function()
{
    $('.carousel').carousel();
    setInterval(function() {
        $('.carousel').carousel('next');
    }, 5000);
});

$(".dropdown-trigger").dropdown();

document.addEventListener('DOMContentLoaded', function() 
{
    var elems = document.querySelectorAll('.materialboxed');
    var instances = M.Materialbox.init(elems);
});

// Or with jQuery
$(document).ready(function()
{
    $('.materialboxed').materialbox();
});

document.addEventListener('DOMContentLoaded', function() 
{
	var elems = document.querySelectorAll('.scrollspy');
    var instances = M.ScrollSpy.init(elems);
});

// Or with jQuery
$(document).ready(function()
{
    $('.scrollspy').scrollSpy();
});

// Scroll To Top Button
function topFunction()
{
	document.body.scrollTop = 0; //For Safari
	document.documentElement.scrollTop = 0; //For Chrome, Firefox, IE and Opera
}

document.addEventListener('DOMContentLoaded', function()
{
    var elems = document.querySelectorAll('.tooltipped');
    var instances = M.Tooltip.init(elems);
});

// Or with jQuery
$(document).ready(function(){
	$('.tooltipped').tooltip();
});

document.addEventListener('DOMContentLoaded', function()
{
    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems);
});

// Or with jQuery
$(document).ready(function()
{
	$('.modal').modal();
});

var instance = M.Carousel.init({
    fullWidth: true
  });

  // Or with jQuery

  $('.carousel.carousel-slider').carousel({
    fullWidth: true
  });

  // Lazyload Remastered

$(function() {
    $("img.lazyload").lazyload();
});

var elem = document.querySelector('.collapsible.expandable');
var instance = M.Collapsible.init(elem, {
  accordion: false
});

$(window).ready(function(){
   if( $(document).scrollTop() < 250 ) {
      $('#hideOnTop').hide();
   } else {
      $('#hideOnTop').show();
   }
});

$(window).scroll(function(){
   if( $(document).scrollTop() < 250 ) {
      $('#hideOnTop').hide(500);
   } else {
      $('#hideOnTop').show(500);
   }
});